class Student
  attr_reader :courses, :first_name, :last_name
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end 
  
  def name 
    @first_name + " " + @last_name
  end 
  
  def enroll(course)
    return "already enrolled" if course.students.include?(self)
    @courses.each {|c| raise "conflict" if c.conflicts_with?(course)}
    @courses << course 
    course.students << self 
  end 
  
  def course_load
    hash = Hash.new(0)
    @courses.each {|c| hash[c.department] += c.credits}
    hash 
  end 
end
